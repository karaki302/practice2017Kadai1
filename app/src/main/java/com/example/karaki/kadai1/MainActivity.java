package com.example.karaki.kadai1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = (Button)findViewById(R.id.button);
        Button button2  = (Button)findViewById(R.id.button2);
        final EditText editText = (EditText)findViewById(R.id.editText);

        button.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View view){
                    Toast.makeText(view.getContext(), editText.getText().toString(), Toast.LENGTH_LONG).show();
                }
        });
        button2.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view){
                    editText.setText("");
                }
        });
    }
}
